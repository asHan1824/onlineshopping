
package com.org.onlineshoppingapplication.service;


import java.util.List;


import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;


public interface ProductService {


    List<Product> products(String emailId) throws UserNotLoginException, CustomException;


}
 







