package com.org.onlineshoppingapplication.service;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.repository.RegisterRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;


@Service
public class RegisterServiceImpl implements RegisterService{

	@Autowired
	RegisterRepository registerRepository;
	
	
	/**
	 * @param user registration details storing to daatabase
	 * @throws UserUtility.EMAIL_IS_PRESENT
	 * @throws UserUtility.USER_ID_IS_PRESENT
	 * @return SUCCESSSFULLY_SAVED_DATA
	 */
	
	@Override
	public RegisterResponse register(UserDto userDto) throws RegisterException {
		String email=userDto.getEmailId();
		
		User user= new User();
				
		 BeanUtils.copyProperties(userDto, user);
		 User userIn=registerRepository.findByEmailId(email);
		 if(ObjectUtils.isEmpty(userIn)) {
		 registerRepository.save(user);
		 
		return new RegisterResponse(UserUtility.SUCCESSSFULLY_SAVED_DATA,HttpStatus.CREATED.value());
		 }
		else
		{
//			throw new RegisterException("user already exist");
			return new RegisterResponse(UserUtility.USER_NOT_FOUND,HttpStatus.BAD_REQUEST.value());
		}
	}


	
	



}
