package com.org.onlineshoppingapplication.service;


import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.exception.RegisterException;

public interface RegisterService {
	

	public RegisterResponse register(UserDto userDto) throws RegisterException;

	

}
