package com.org.onlineshoppingapplication.service;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

 

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.ResponseDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.repository.LoginRepository;

 

/**
 * @author Uma
 *
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    LoginRepository loginRepository;

 

    @Override
    public ResponseDto userLogin(LoginDto loginDto) {
        ResponseDto responseDto = new ResponseDto();
        if (loginDto.getEmailId().isEmpty() || loginDto.getPassword().isEmpty()) {
            responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setStatusMessage("Please provide the required  data");
        } else {
            User user = loginRepository.findByEmailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword());

 

            if (user != null) {
                responseDto.setStatusCode(HttpStatus.OK.value());
                responseDto.setStatusMessage("Logged in successfully!!..");
            } else {
                responseDto.setStatusCode(HttpStatus.NOT_FOUND.value());
                responseDto.setStatusMessage("Please Register");
            }
        }
        return responseDto;
    }

 

}
 