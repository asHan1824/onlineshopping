package com.org.onlineshoppingapplication.service;

 

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.ResponseDto;

 

/**
 * @author Uma
 *
 */
public interface LoginService {

 

    public ResponseDto userLogin(LoginDto loginDto);

 

}