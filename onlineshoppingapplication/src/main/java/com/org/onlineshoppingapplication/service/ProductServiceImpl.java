package com.org.onlineshoppingapplication.service;

 

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;
import com.org.onlineshoppingapplication.repository.ProductRepository;
import com.org.onlineshoppingapplication.repository.UserRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;

 

 

@Service
public class ProductServiceImpl implements ProductService{

 

    @Autowired
    ProductRepository productRepository;
    
    @Autowired
    UserRepository userRepository;
    
    /**
     * @author Koushik
     * @since 23-03-2020
     * @param emailId
     * @return  -List of products
     * @throws CustomException 
     * @throws UserNotLoginException 
     * 
     */
    @Override
    public List<Product> products(String emailId) throws CustomException {
    
        String userType = userRepository.getProductType(emailId);
        if(userType!=null) {
        System.out.println(userType);
        List<Product> product;
        if(userType.equalsIgnoreCase("low")) {
            product =    productRepository.getProductDetails(userType);
        }
        else
        {
            product = productRepository.findAll();
        }
        return product;
        }
        else {
            throw new CustomException(UserUtility.USER_NOT_EXIST_STATUS);
        }
        
        
    }

 

}
 