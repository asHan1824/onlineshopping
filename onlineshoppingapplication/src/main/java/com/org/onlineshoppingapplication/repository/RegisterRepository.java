package com.org.onlineshoppingapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.org.onlineshoppingapplication.entity.User;

public interface RegisterRepository extends JpaRepository<User, Integer> {

	User findByEmailId(String email);

	
}
