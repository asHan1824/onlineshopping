package com.org.onlineshoppingapplication.repository;

 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

 

import com.org.onlineshoppingapplication.entity.User;

 


public interface UserRepository extends JpaRepository<User, Long>{

 

    
    @Query("select entity.userType from User entity where entity.emailId=?1")
    public String getProductType(String emailId);
}