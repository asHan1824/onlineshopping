package com.org.onlineshoppingapplication.repository;

 

import java.util.List;

 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

 

import com.org.onlineshoppingapplication.entity.Product;

 

public interface ProductRepository extends JpaRepository<Product, Long>{

 

    @Query("select entity from Product entity where entity.productType=?1")
    public List<Product> getProductDetails(String productType);
    
    
    
}