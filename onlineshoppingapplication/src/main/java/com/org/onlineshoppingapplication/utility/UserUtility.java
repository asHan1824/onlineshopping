package com.org.onlineshoppingapplication.utility;

public class UserUtility {
	
	    public static final String SUCCESSSFULLY_SAVED_DATA="User registered Successfully!";
	    public static final String USER_NOT_FOUND="user already exist ";
	    public static final String USER_WELCOME="You have successfully logged in";
		public static String USER_ERROR_STATUS="User not found";
		public static int USER_STATUSCODE_ERROR= 705;
		public static final String USER_NOT_LOGIN = "User did not Logged In";
	    public static final int USER_NOT_LOGIN_STATUS = 605;
	    
	    public static final String USER_NOT_EXIST = "User IS not THERE";
	    public static final int USER_NOT_EXIST_STATUS = 606;
	
	    
	    private UserUtility() {
	    	}

}
