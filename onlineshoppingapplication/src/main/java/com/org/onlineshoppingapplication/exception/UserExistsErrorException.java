package com.org.onlineshoppingapplication.exception;

public class UserExistsErrorException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public UserExistsErrorException(String message) {
		super(message);
	}

}
