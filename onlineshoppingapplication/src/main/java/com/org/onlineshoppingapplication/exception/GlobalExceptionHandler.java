package com.org.onlineshoppingapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.org.onlineshoppingapplication.utility.UserUtility;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(UserExistsErrorException.class)
	public ResponseEntity<ErrorResponse> userExistsErrorException(UserExistsErrorException ex) {

		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatus(UserUtility.USER_STATUSCODE_ERROR);

		
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}

}