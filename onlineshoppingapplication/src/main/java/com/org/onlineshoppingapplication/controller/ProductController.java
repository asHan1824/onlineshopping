
    
package com.org.onlineshoppingapplication.controller;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;
import com.org.onlineshoppingapplication.service.ProductService;


@RestController
@RequestMapping("/product")
public class ProductController {
    
    @Autowired
    ProductService productService;


    /**
     * @author Koushik
     * @since 23-03-2020
     * @param emailId
     * @return productdto  -List of products
     * @throws UserNotLoginException 
     * @throws CustomException 
     */
    @GetMapping("/products/{emailId}")
    public ResponseEntity<List<Product>> products(@PathVariable String emailId) throws UserNotLoginException, CustomException{
        List<Product> productdto = productService.products(emailId);
        return new ResponseEntity<>(productdto, HttpStatus.OK);
    }
}
 







