package com.org.onlineshoppingapplication.controller;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

 

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.ResponseDto;
import com.org.onlineshoppingapplication.service.LoginService;

 

@RestController
public class LoginController {
    @Autowired
    LoginService loginService;

 

    /**
     * This method is used to validate the user login
     * 
     * @author Uma
     * @since 2020-03-23
     * @param LoginDto-contains the request params EmailId and password ResponseDto-
     *                          contains success or failure message as a response
     * @return ResponseEntity Object along with status code and message
     */

 

    @PostMapping("users/login")
    public ResponseEntity<ResponseDto> userLogin(@RequestBody LoginDto loginDto) {
        ResponseDto responseDto = loginService.userLogin(loginDto);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

 

}
 