package com.org.onlineshoppingapplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.service.RegisterService;

@RestController
public class RegisterController {
	
	@Autowired
	RegisterService registerService;
	
	/**
	 *
	 * @param information storing to database
	 * @return the Success message with corresponding status code
	 */
	@PostMapping("/register")
	public ResponseEntity<RegisterResponse> register(@Valid @RequestBody UserDto userDto) throws RegisterException{
		return new ResponseEntity<>(registerService.register(userDto), HttpStatus.CREATED);
	}


}
