package com.org.onlineshoppingapplication.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.repository.RegisterRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
class RegisterServiceTestCase {
	
	
	@Mock
	RegisterRepository registerRepository;
	
	@InjectMocks
	RegisterServiceImpl registerServiceImpl;
	
	RegisterResponse response = new RegisterResponse("Account registered Successfully", HttpStatus.CREATED.value());
		
	
	User userDto = new User();
	

	@Before
	public void setup() {
          userDto.setUserName("John");
          userDto.setEmailId("john@gmail.com");
          userDto.setAddress("mettur");
          userDto.setPassword("abc");
          userDto.setMobileNumber(9876543210l);
          userDto.setRegisterId(1);
          userDto.setUserType("p");
          
          
	}	
	

	@Test
	public void testRegister() {		
		UserDto accountRegistrationDto = new UserDto();

		Mockito.when(registerRepository.save(userDto)).thenReturn(userDto);
		RegisterResponse registerResponse = registerServiceImpl.register(accountRegistrationDto);
		assertEquals(HttpStatus.CREATED.value(), registerResponse.getStatusCode());
	}	

	
	

	@Test
	void test() {
		fail("Not yet implemented");
	}

}
