package com.org.onlineshoppingapplication.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.service.RegisterService;

@RunWith(MockitoJUnitRunner.Silent.class)
class RegisterControllerTestCase {
	
	
	@InjectMocks
	RegisterController registerController;

	@Mock
	RegisterService registerService;
	
UserDto userDto = new UserDto();
	

	RegisterResponse response = new RegisterResponse("User registered Successfully", HttpStatus.CREATED.value());

	@Before
	public void setup() {
          userDto.setUserName("John");
          userDto.setEmailId("john@gmail.com");
          userDto.setAddress("mettur");
          userDto.setPassword("abc");
          userDto.setMobileNumber(9876543210l);
          userDto.setRegisterId(1);
          userDto.setUserType("p");
          
          
	}	

	
	
	@Test 
	public void registerTest() {
		Mockito.when(registerService.register(userDto)).thenReturn(response);
		ResponseEntity<RegisterResponse> expected = registerController.register(userDto);		
		assertEquals(HttpStatus.CREATED, expected.getStatusCode());
		//verify(registerService, times(1)).register(userDto);
	}
	
	@Test
	void test() {
		fail("Not yet implemented");
	}

}
